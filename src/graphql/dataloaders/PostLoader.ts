import { PostModel, PostInstance } from '../../models/PostModel';
import { FindOptions } from 'sequelize';
import { DataLoaderParam } from '../../interfaces/DataLoaderParamInterface';
import { RequestedFields } from '../ast/RequestedFields';

export class PostLoader {

    static batchPosts(Post: PostModel, params: DataLoaderParam<number>[], requestedFields: RequestedFields): Promise<PostInstance[]> {
        
        const ids: number[] = params.map(param => param.key);

        return Promise.resolve(
            Post.findAll({
                where: <FindOptions<PostInstance>>{ id: { $in: ids } },
                attributes: requestedFields.getFields(params[0].info, { keep: ['id'], exclude: ['comments'] })
            })
        );
    }
}
