import { GraphQLResolveInfo } from 'graphql';

import { DbConnection } from '../../../interfaces/DbConnectionInterface';
import { PostInstance } from '../../../models/PostModel';
import { Transaction } from 'sequelize';
import { handleError, throwError } from '../../../utils/utils';
import { compose } from '../../composable/composable.resolver';
import { authResolvers } from '../../composable/auth.resolver';
import { AuthUser } from '../../../interfaces/AuthUserInterface';
import { DataLoaders } from '../../../interfaces/DataLoadersInterface';
import { ResolverContext } from '../../../interfaces/ResolverContextInterface';

export const postResolvers ={

    Post: {
        author: (post, _args, { dataloaders: { userLoader } }: { dataloaders: DataLoaders }, info: GraphQLResolveInfo) => {
            return userLoader
                .load({ key: post.get('author'), info })
                .catch(handleError);
        },
        comments: (post, { first = 10, offset = 0 }, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Comment
                .findAll({
                    where: { post: post.get('id') },
                    limit: first,
                    attributes: context.requestedFields.getFields(info),
                    offset
                })
                .catch(handleError);
        }
    },
    Query: {
        posts: (_parent, { first = 10, offset = 0 }, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Post
                .findAll({
                    limit: first,
                    attributes: context.requestedFields.getFields(info, { keep: ['id'], exclude: ['comments']}),
                    offset
                })
                .catch(handleError);
        },

        post: (_parent, { id }, context: ResolverContext, info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return context.db.Post
                .findById(id, {
                    attributes: context.requestedFields.getFields(info, { keep: ['id'], exclude: ['comments']}),
                })
                .then((post: PostInstance) => {
                    throwError(!post, `Post with id ${id} not found!`);
                    return post;
                }).catch(handleError);
        }
    },

    Mutation: {
        createPost: compose(...authResolvers)((_parent, { input }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            input.author = authUser.id;
            return db.sequelize.transaction((transaction: Transaction) => {
                return db.Post.create(input, { transaction });
            }).catch(handleError);
        }),
        updatePost: compose(...authResolvers)((_parent, { id, input }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            id = parseInt(id)
            return db.sequelize.transaction((transaction: Transaction) => {
                return db.Post
                    .findById(id)
                    .then((post: PostInstance) => {
                        throwError(!post, `Post with id ${id} not found!`);
                        throwError(post.get('author') !== authUser.id, `Unauthorized! You can only edit posts by yourself!`);
                        input.author = authUser.id;
                        return post.update(input, { transaction });
                    })
            }).catch(handleError);
        }),
        deletePost: compose(...authResolvers)((_parent, { id }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return db.sequelize.transaction((transaction: Transaction) => {
                return db.Post
                    .findById(id)
                    .then((post: PostInstance) => {
                        throwError(!post, `Post with id ${id} not found!`);
                        throwError(post.get('author') !== authUser.id, `Unauthorized! You can only edit posts by yourself!`);
                        return post.destroy({ transaction })
                            .then(post => !!post);
                    })
            }).catch(handleError);
        })
    }
};