import { GraphQLResolveInfo } from 'graphql';
import { Transaction } from 'sequelize';

import { DbConnection } from '../../../interfaces/DbConnectionInterface';
import { CommentInstance } from '../../../models/CommentModel';
import { handleError, throwError } from '../../../utils/utils';
import { authResolvers } from '../../composable/auth.resolver';
import { compose } from '../../composable/composable.resolver';
import { AuthUser } from '../../../interfaces/AuthUserInterface';
import { DataLoaders } from '../../../interfaces/DataLoadersInterface';
import { ResolverContext } from '../../../interfaces/ResolverContextInterface';

export const commentResolvers = {

    Comment: {
        user: (comment, _args, { dataloaders: {userLoader} } : { dataloaders: DataLoaders }, info: GraphQLResolveInfo) => {
            return userLoader
                .load({ key: comment.get('user'), info })
                .catch(handleError);
        },
        post: (comment, _args, { dataloaders: { postLoader } } : { dataloaders: DataLoaders }, info: GraphQLResolveInfo) => {
            return postLoader
                .load({ key: comment.get('post'), info })
                .catch(handleError);
        }
    },
    Query: {
        commentsByPost: compose()((_parent, { postId, first = 10, offset = 0 }, context: ResolverContext, info: GraphQLResolveInfo) => {
            postId = parseInt(postId);
            return context.db.Comment
                .findAll({
                    where: { post: postId },
                    limit: first,
                    attributes: context.requestedFields.getFields(info, { keep: undefined }),
                    offset
                })
                .catch(handleError);
        })
    },
    Mutation: {
        createComment: compose(...authResolvers)((_parent, { input }, { db, authUser } : { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            input.user = authUser.id;
            return db.sequelize.transaction((transaction:  Transaction) => {
                return db.Comment
                    .create(input, { transaction });
            })
            .catch(handleError);
        }),
        updateComment: compose(...authResolvers)((_parent, { id, input }, { db, authUser } : { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return db.sequelize.transaction((transaction:  Transaction) => {
                return db.Comment
                    .findById(id)
                    .then((comment: CommentInstance) => {
                        throwError(!comment, `Comment with id ${id} not found!`);
                        throwError(comment.get('user') !== authUser.id, `Unauthorized! You can only edit comments by yourself!`);
                        input.user = authUser.id;
                        return comment.update(input, { transaction });
                    });
            })
            .catch(handleError);
        }),
        deleteComment: compose(...authResolvers)((_parent, { id }, { db, authUser } : { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return db.sequelize.transaction((transaction:  Transaction) => {
                return db.Comment
                    .findById(id)
                    .then((comment: CommentInstance) => {
                        throwError(!comment, `Comment with id ${id} not found!`);
                        throwError(comment.get('user') !== authUser.id, `Unauthorized! You can only delete comments by yourself!`);

                        return comment.destroy({ transaction })
                            .then(comment => !!comment);
                    });
            })
            .catch(handleError);
        })
    }
};