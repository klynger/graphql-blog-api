import { GraphQLResolveInfo } from 'graphql';
import { Transaction, WhereOptions } from 'sequelize';

import { AuthUser } from '../../../interfaces/AuthUserInterface';
import { DbConnection } from '../../../interfaces/DbConnectionInterface';
import { UserInstance } from '../../../models/UserModel';
import { handleError, throwError } from '../../../utils/utils';
import { compose } from '../../composable/composable.resolver';
import { authResolvers } from '../../composable/auth.resolver';
import { ResolverContext } from '../../../interfaces/ResolverContextInterface';

export const userResolvers = {

    User: {
        posts: (user, { first = 10, offset = 0 }, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.Post
                .findAll({
                    where: <WhereOptions<UserInstance>>{ author: user.get('id') },
                    limit: first,
                    attributes: context.requestedFields.getFields(info, { keep: ['id'], exclude: ['comments'] }),
                    offset
                }).catch(handleError);
        }
    },
    Query: {
        users: (_parent, { first = 10, offset = 0 }, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.User
                .findAll({
                    limit: first,
                    attributes: context.requestedFields.getFields(info, { keep: ['id'], exclude: ['posts'] }),
                    offset
                }).catch(handleError);
        },

        user: (_parent, { id }, context: ResolverContext, info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return context.db.User
                .findById(id, {
                    attributes: context.requestedFields.getFields(info, { keep: ['id'], exclude: ['posts'] })
                })
                .then((user: UserInstance) => {
                    throwError(!user, `User with id ${id} not found!`);
                    return user;
                }).catch(handleError);
        },

        currentUser: compose(...authResolvers)((_parent, _args, context: ResolverContext, info: GraphQLResolveInfo) => {
            return context.db.User
                .findById(context.authUser.id, {
                    attributes: context.requestedFields.getFields(info, { keep: ['id'], exclude: ['posts'] })
                })
                .then((user: UserInstance) => {
                    throwError(!user, `User with id ${context.authUser.id} not found!`);
                    return user;
                })
                .catch(handleError);
        })
    },
    Mutation: {
        createUser: (_parent, { input }, { db }: { db: DbConnection }, _info: GraphQLResolveInfo) => {
            return db.sequelize.transaction((t: Transaction) => {
                return db.User
                    .create(input, { transaction: t });
            }).catch((error: Error) => {
                if(error.name === 'SequelizeUniqueConstraintError') {
                    error.name = 'DuplicateEmail';
                    error.message = 'Email already taken';
                }
                return handleError(error);
            });
        },
        updateUser: compose(...authResolvers)((_parent, { input }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            return db.sequelize.transaction((t: Transaction) => {
                return db.User
                    .findById(authUser.id)
                    .then((user: UserInstance) => {
                        throwError(!user, `User with id ${authUser.id} not found!`);
                        return user.update(input, { transaction: t });
                    });
            }).catch(handleError);
        }),
        updateUserPassword: compose(...authResolvers)((_parent, { input }, { db, authUser }: { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            return db.sequelize.transaction((t: Transaction) => {
                return db.User
                    .findById(authUser.id)
                    .then((user: UserInstance) => {
                        throwError(!user, `User with id ${authUser.id} not found!`);
                        return user.update(input, { transaction: t })
                            .then((user: UserInstance) => !!user);
                    });
            }).catch(handleError);
        }),
        deleteUser: compose(...authResolvers)((_parent, _args, { db, authUser }: { db: DbConnection, authUser: AuthUser }, _info: GraphQLResolveInfo) => {
            return db.sequelize.transaction((t: Transaction) => {
                return db.User
                    .findById(authUser.id)
                    .then((user: UserInstance) => {
                        throwError(!user, `User with id ${authUser.id} not found!`);
                        return user.destroy({ transaction: t })
                            .then(user => !!user)
                    });
            }).catch(handleError);
        })
    }
};